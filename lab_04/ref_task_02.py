import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = ((y + (sin(x))^2) / (x - y)^7) + 5*y
Z2  = (x^2 + cos((0.7*x)^-4) - 15
Z3 = |(Z1 - 10) / Z2|

Запахи
світч
довгий метод

обєднання умовних операторів
виділення методів
перенесення методів між класами
виділення полів
'''
class math_program:
    val = None
    z1 = None
    z2 = None
    z3 = None
    d = None

    def input(self):
        try:
            self.x = float(input("Введіть значення x: "))
            self.y = float(input("Введіть значення y: "))
        except ValueError:
            print("Некоректні дані. Введіть числові значення для x та y.")
            sys.exit(1)

    def calculation(self):
        self.d = (self.x - self.y) ** 7

        self.z1 = ((self.y + math.sin(self.x) ** 2) / self.d) + 5 * self.y
        self.z2 = self.x ** 2 + math.cos((0.7 * self.x) ** -4) - 15
        self.z3 = abs((self.z1 - 10) / self.z2)

    def check_validation(self):
        message = None

        if (self.x - self.y) ** 7 == 0:
            message = "Некоректні дані. Знаменник не може дорівнювати нулю."
        #if isinstance(self.z1, float):
            #message = "Некоректні дані. Значення Z1 має бути числом."
        if self.z2 == 0:
            message = "Некоректні дані. Значення Z2 не може бути рівним нулю."

        if message:
            print(message)
            sys.exit(1)

    def print_result(self):
        print(f"Z1 = {self.z1}")
        print(f"Z2 = {self.z2}")
        print(f"Z3 = {self.z3}")
        sys.exit(0)
def main():
    obj_class = math_program()
    obj_class.input()
    obj_class.check_validation()
    obj_class.calculation()
    obj_class.print_result()


if __name__ == "__main__":
    main()