import sys
import math

class MainClassApp:
    v1 = None
    v2 = None

    def get_validated_float(self, val):
        try:
            val = float(val)
        except ValueError:
            print("Введене значення не є числом.")
            sys.exit(1)
        return val

    def input_val(self):
        self.v1 = self.get_validated_float(input('Введіть радіус основи:'))
        self.v2 = self.get_validated_float(input('Введіть висоту:'))

    def validate_radius(self):
        if self.v1 <= 0:
            print(f"Довжина радіусу основи ({self.v1}) не може бути менше або дорівнювати 0.")
            sys.exit(1)

    def validate_height(self):
        if self.v2 <= 0:
            print(f"Довжина висоти ({self.v2}) не може бути менше або дорівнювати 0.")
            sys.exit(1)

    def search_v(self):
        v3 = (math.pi * (self.v1 ** 2) * self.v2) / 3
        print(f"Об’єм конуса з радіусом основи {self.v1} та висотою {self.v2} дорівнює {v3}.")
        sys.exit(0)

def main():
    calc = MainClassApp()
    calc.input_val()
    calc.validate_radius()
    calc.validate_height()
    calc.search_v()

if __name__ == '__main__':
    main()