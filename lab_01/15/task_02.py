import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = ((x + 4) / 2 - y / (4 * x)^1/2)
Z2 = 1 / (x + y)
Z3 = Z1 / Z2
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        d = (4 * x) ** 0.5
        if (4 * x) ** 0.5 != 0:
            z1 = ((x + 4) / 2) - (y / d)
            if (x + y) != 0:
                z2 = 1 / (x + y)
                if z2 != 0:
                    z3 = (((x + 4) / 2) - (y / (4 * x) ** 0.5)) / (1 / (x + y))
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Знаменник Z2 не може дорівнювати нулю.")
                    sys.exit(1)
            else:
                print("Некоректні дані. Знаменник не може дорівнювати нулю.")
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
