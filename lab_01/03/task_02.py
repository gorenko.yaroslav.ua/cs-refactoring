import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = ((y + (sin(x))^2) / (x - y)^7) + 5*y
Z2  = (x^2 + cos((0.7*x)^-4) - 15
Z3 = |(Z1 - 10) / Z2|
'''
def input_num():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        return x, y
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


def validate_d(x, y):
    d = (x - y) ** 7

    if d == 0:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
    return d


def calc(x, y, d):
    try:
        z1 = ((y + math.sin(x) ** 2) / d) + 5 * y
        z2 = x ** 2 + math.cos((0.7 * x) ** -4) - 15  # ZeroDivisionError
        z3 = abs((z1 - 10) / z2)

    except ZeroDivisionError:
        print("Некоректні дані. Значення Z2 не може бути рівним нулю.")
        sys.exit(1)

    return z1, z2, z3


def output(z1, z2, z3):
    print(f"Z1 = {z1}")
    print(f"Z2 = {z2}")
    print(f"Z3 = {z3}")
    sys.exit(0)


def main():
    x, y = input_num()
    d = validate_d(x, y)
    z1, z2, z3 = calc(x,y, d)
    output(z1, z2, z3)

    # try:
    #     x = float(input("Введіть значення x: "))
    #     y = float(input("Введіть значення y: "))
    #     d = (x - y) ** 7
    #     if d != 0:
    #         z1 = ((y + math.sin(x) ** 2) / d) + 5 * y
    #         z2 = x ** 2 + math.cos((0.7 * x) ** -4) - 15
    #         if isinstance(z1, float) and z2 != 0:
    #             z3 = abs((z1 - 10) / z2)
    #             print(f"Z1 = {z1}")
    #             print(f"Z2 = {z2}")
    #             print(f"Z3 = {z3}")
    #             sys.exit(0)
    #         else:
    #             print("Некоректні дані. Значення Z2 не може бути рівним нулю.")
    #             sys.exit(1)
    #     else:
    #         print("Некоректні дані. Знаменник не може дорівнювати нулю.")
    #         sys.exit(1)
    # except ValueError:
    #     print("Некоректні дані. Введіть числові значення для x та y.")
    #     sys.exit(1)


if __name__ == "__main__":
    main()
