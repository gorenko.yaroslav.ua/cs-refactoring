import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1 = (2 * (sin(2x))^2) / ((cos(x + 2*y))^2)
Z2 = sin(5- 8*x)
Z3 = (Z2 - Z1) / 2
'''


def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))
        if (math.cos(x + 2 * y)) ** 2 != 0:
            z1 = (2 * (math.sin(2 * x)) ** 2) / (math.cos(x + 2 * y)) ** 2
            z2 = math.sin(5 - 8 * x)
            z3 = (math.sin(5 - 8 * x) - (2 * (math.sin(2 * x)) ** 2) / (math.cos(x + 2 * y)) ** 2) / 2
            print(f"Z1 = {z1}")
            print(f"Z2 = {z2}")
            print(f"Z3 = {z3}")
            sys.exit(0)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)


if __name__ == "__main__":
    main()
